<?php


namespace App\Command;

use App\Services\WeatherConfigService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use App\Services\OpenWeatherService;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use App\Services\GeoLocationService;

class ForecastCommand extends Command
{
    protected static $defaultName = 'app:weather-forecast';
    /**
     * @var OpenWeatherService
     */
    private $openWeatherService;

    /**
     * @var WeatherConfigService
     */
    private $weatherConfigService;

    /**
     * @var GeoLocationService
     */
    private $geoLocationService;

    protected function configure(): void
    {
        $this->setDescription('Get weather forecast for a location')
            ->setHelp('This command allows you to get weather forecast for a location, by zip-code or city name 
            and returns temperatures in c for Celsius or fr for Fahrenheit')
            ->addOption('city', 'c',
                InputOption::VALUE_REQUIRED,'This flag allows you to enter the name of the city. Ex. --city=Skopje')
            ->addOption('zipCode', 'z',
                InputOption::VALUE_REQUIRED,'This flag allows you to enter the zip code of the city. Ex. --zipCode=1000')
            ->addOption('temperature', 't',
                InputOption::VALUE_REQUIRED, 'This flag allows you to enter the unit in which temperature will be shown . Ex. --temperature=fr', 'c')
            ->addOption('latest', 'l',
                InputOption::VALUE_NONE,'This flag allows you to specify if you want to get the forecast for the latest entered city. Ex. --latest')
            ->addOption('import', 'i',
                InputOption::VALUE_REQUIRED,'This flag allows you to specify the config file path for max 10 cities in one call. Ex --import')
            ->addOption('disableAutoLocation', 'dl',
                InputOption::VALUE_NONE,'When specified this flag allows you to disabled auto location. Ex --disableAutoLocation');
    }

    /**
     * ForecastCommand constructor.
     * @param OpenWeatherService $openWeatherService
     * @param WeatherConfigService $weatherConfigService
     * @param GeoLocationService $geoLocation
     */
    public function __construct(OpenWeatherService $openWeatherService, WeatherConfigService $weatherConfigService, GeoLocationService $geoLocation)
    {
        $this->openWeatherService = $openWeatherService;
        $this->weatherConfigService = $weatherConfigService;
        $this->geoLocationService = $geoLocation;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $helper = $this->getHelper('question');
        $question = new Question(
            'Please enter city or zip code and unit type. Example: Skopje,fr: '
        );
        $question->setValidator(function ($answer) {
            if (!is_string($answer) || count(explode(',', $answer)) !== 2) {
                throw new \RuntimeException(
                    'The format that you entered is wrong. Example expected format Paris,fr'
                );
            }
            $answerArray = explode(',', $answer);
            try {
                return $this->weatherConfigService->saveDefaultWeatherConfig($answerArray[1], $answerArray[0]);
            } catch (\Exception $e) {
                throw new \RuntimeException($e->getMessage());
            }
        });
        try {
            $configFilePath = null;

            if (!$input->getOption('city') && !$input->getOption('zipCode') ) {
                if ($input->getOption('latest')) {
                    $configFilePath = $this->weatherConfigService->getDefaultWeatherConfigFile();
                } else if ($input->getOption('import')) {
                    $configFilePath = $input->getOption('import');
                } else if (!$input->getOption('disableAutoLocation')) {
                    $autoLocation = $this->geoLocationService->getCurrentLocation();
                    $autoLocationQuestion = new ConfirmationQuestion(
                        'Do you want weather forecast for ' . $autoLocation->city, true
                    );
                    $answer = $helper->ask($input, $output, $autoLocationQuestion);
                    if ($answer) {
                        try {
                            $configFilePath = $this->weatherConfigService->saveDefaultWeatherConfig($input->getOption('temperature'), $autoLocation->city);
                        } catch (\Exception $e) {
                            $output->writeln($e->getMessage());
                            return 1;
                        }
                    }
                }
            } else {
                $configFilePath = $this->weatherConfigService->saveDefaultWeatherConfig($input->getOption('temperature'), $input->getOption('city') ?? $input->getOption('zipCode'));
            }

            if (empty($configFilePath)) {
                $configFilePath = $helper->ask($input, $output, $question);
            }

            $weatherConfigs = $this->weatherConfigService->mapConfigFileToWeatherConfigs($configFilePath);


            foreach ($weatherConfigs as $weatherConfig) {
                try {
                    $weatherForecast = $this->openWeatherService->getWeatherForecastForWeatherConfig($weatherConfig);
                    $msg = 'Temperature in ' . $weatherForecast->name . ' is ' . $weatherForecast->main->temp . $input->getOption('temperature') . '. Expect ' . $weatherForecast->weather[0]->description . ' with ' . $weatherForecast->main->humidity . ' % humidity in the next hours.';
                    $output->writeln($msg);
                }catch (\Exception $exception) {
                    $output->writeln('<error>' . $exception->getMessage() . '</error>');
                }

            }
            return 0;
        } catch (\Exception $exception) {
            $output->writeln('<error>' . $exception->getMessage() . '</error>');
            return 1;
        }
    }
}