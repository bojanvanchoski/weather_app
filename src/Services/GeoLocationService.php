<?php

namespace App\Services;

use GuzzleHttp\Client;

class GeoLocationService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'http://ip-api.com/json']);
    }

    public function getCurrentLocation()
    {
        $request = $this->client->request('GET');
        $body = $request->getBody();
        $response = '';
        while (!$body->eof()) {
            $response .= $body->read(1024);
        }

        return json_decode($response);
    }
}