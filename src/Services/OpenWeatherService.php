<?php

namespace App\Services;

use App\Entity\WeatherConfig;
use GuzzleHttp\Client;

class OpenWeatherService
{
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => 'api.openweathermap.org/data/2.5/']);
    }

    public function getWeatherForecastForWeatherConfig(WeatherConfig $weatherConfig)
    {
        try {
            $request = $this->client->request('GET', 'weather', ['query' => [
                    'q' => $weatherConfig->getQuery(),
                    'appid' => $_ENV['OPEN_WEATHER_API_KEY'],
                    'units' => $weatherConfig->getUnit()
                ]
                ]
            );
        } catch (\Exception $e) {
            if ($e->getCode() === 404) {
                throw new \InvalidArgumentException('There were no results found for: ' . $weatherConfig->getQuery());
            } else {
                throw  $e;
            }
        }
        $body = $request->getBody();
        $response = '';
        while (!$body->eof()) {
            $response .= $body->read(1024);
        }

        return json_decode($response);
    }
}