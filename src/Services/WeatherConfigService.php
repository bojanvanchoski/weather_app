<?php

namespace App\Services;

use App\Entity\WeatherConfig;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xls;
use Symfony\Component\Filesystem\Filesystem;

class WeatherConfigService
{
    private $fileSystem;

    public function __construct(Filesystem $fileSystem)
    {
        $this->fileSystem = $fileSystem;
    }

    public function saveDefaultWeatherConfig(string $tempFormat, string $query): string
    {
        $this->validateAndFormatWeatherConfigInput($tempFormat, $query);
        $configFilepath = $this->getDefaultConfigFilePath();
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xls();

        try {
            $spreadsheet = $reader->load($configFilepath);
        } catch (\InvalidArgumentException $e) {
            $spreadsheet = new Spreadsheet();
        }

        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', $query);
        $sheet->setCellValue('B1', $tempFormat);
        $writer = new Xls($spreadsheet);
        $writer->save($configFilepath);

        return $configFilepath;
    }

    public function getDefaultWeatherConfigFile(): ?string
    {
        $filePath = $this->getDefaultConfigFilePath();

        return $this->fileSystem->exists($filePath) ? $filePath : null;
    }

    public function mapConfigFileToWeatherConfigs(string $configFilePath): array
    {
        if (!$this->fileSystem->exists($configFilePath)) {
            throw new \Exception('Config file not found');
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($configFilePath);

        $weatherConfigArray = $spreadsheet->getActiveSheet()->toArray();
        if (count($weatherConfigArray) > 10) {
            throw new \Exception('Config file can not have more then 10 records');
        }
        return array_map(function ($item) {
            try {
                list($tempFormat, $query) = $this->validateAndFormatWeatherConfigInput($item[1], $item[0]);
                $weatherConfig = new WeatherConfig();
                $weatherConfig->setQuery($query);
                $weatherConfig->setUnit($tempFormat);
                return $weatherConfig;
            } catch (\InvalidArgumentException $exception) {
                throw new \Exception('Invalid data in config file');
            }
        }, $weatherConfigArray);
    }

    private function getDefaultConfigFilePath(): string
    {
        if(!empty($_ENV['WF_CONFIG_DIR'])) {
            $baseUrl = $_ENV['WF_CONFIG_DIR'];
        } else{
            $baseUrl =  $_SERVER['HOME'] ?? $_SERVER['HOMEDRIVE'].$_SERVER['HOMEPATH'];
        }
        return $baseUrl . DIRECTORY_SEPARATOR . "weather_default_config.xls";
    }

    private function validateAndFormatWeatherConfigInput(string $tempFormat = null, string $query = null): array
    {
        if (!$query) {
            throw new \InvalidArgumentException('Invalid query');
        }
        if ($tempFormat && in_array($tempFormat, ['fr', 'c'])) {
            $tempFormat = $tempFormat == 'fr' ? 'imperial' : 'metric';
        } else {
            throw new \InvalidArgumentException('Invalid unit');
        }
        return [$tempFormat, $query];
    }
}