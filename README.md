This application requires PHP version of minimum 7.1.3.
After clone / download the application run:
`composer install`
Open cli and run the command :
`bin/console app:weather-forecast`
To see all options for how to use command use flag --help:
`bin/console app:weather-forecast --help`
