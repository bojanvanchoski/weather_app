<?php

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class ForecastCommandTest extends KernelTestCase
{
    public function testAutoLocation()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['yes']);
        $exitCode = $commandTester->execute([], []);
        $output = $commandTester->getDisplay();

        $this->assertStringContainsString("Do you want weather forecast for", $output);
        $this->assertStringContainsString('Temperature in', $output);
        $this->assertEquals(0, $exitCode);
    }

    public function testAutoLocationReject()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $commandTester->setInputs(['no']);
        $commandTester->execute([], []);
        $output = $commandTester->getDisplay();

        $this->assertStringContainsString("Please enter city or zip code and unit type", $output);
    }

    public function testAutoLocationDisabled()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--disableAutoLocation' => true]);
        $output = $commandTester->getDisplay();

        $this->assertStringContainsString("Please enter city or zip code and unit type", $output);
    }

    public function testErrorMessageForInvalidTemperatureUnit()
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $exitCode = $commandTester->execute(['--city' => 'Skopje', '-t' => 'celsuis']);
        $output = $commandTester->getDisplay();

        $this->assertEquals("Invalid unit\n", $output);
        $this->assertEquals(1, $exitCode);
    }

    public function testErrorMessageForInvalidCity()
    {
        $city = 'Skopderje';
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $exitCode = $commandTester->execute(['--city' => $city, '-t' => 'c']);
        $output = $commandTester->getDisplay();

        $this->assertEquals("There were no results found for: " . $city . "\n", $output);
        $this->assertEquals(0, $exitCode);
    }

    public function testExecuteWithCityAndUnitInputSuccess()
    {
        $city = 'Skopje';
        $unit = 'c';
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $exitCode = $commandTester->execute(['--city' => $city, '-t' => $unit]);
        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('Temperature in ' . $city, $output);
        $this->assertEquals(0, $exitCode);
    }

    public function testUseLatestInputAsDefaultSuccess()
    {
        $city = 'Skopje';
        $unit = 'c';
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $exitCode = $commandTester->execute(['--city' => $city, '-t' => $unit]);
        $commandWithLatestOption = $application->find('app:weather-forecast');
        $commandTesterWithLatestOption = new CommandTester($commandWithLatestOption);
        $exitCodeWithLatestOption = $commandTesterWithLatestOption->execute(['-l' => true]);
        $output = $commandTesterWithLatestOption->getDisplay();

        $this->assertStringContainsString('Temperature in ' . $city, $output);
        $this->assertEquals(0, $exitCode);
        $this->assertEquals(0, $exitCodeWithLatestOption);
    }


    public function testPrompInputWhenDefaultConfigNotFound()
    {
        $pathParts = [__DIR__, '..', 'data', 'weather_default_config.xls'];
        $filePath = realpath(implode(DIRECTORY_SEPARATOR, $pathParts));
        unlink(realpath($filePath));

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--latest' => true]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString("Please enter city or zip code and unit type", $output);
    }


    public function testWithImportOptionAndValidFile()
    {
        $pathParts = [__DIR__, '..', 'data', 'weather_config_valid.xls'];
        $filePath = realpath(implode(DIRECTORY_SEPARATOR, $pathParts));

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--import' => $filePath]);
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Temperature in Skopje', $output);
        $this->assertStringContainsString('Temperature in Paris', $output);
        $this->assertStringContainsString('Temperature in Berlin', $output);
    }

    public function testWithImportOptionAndInvalidCityInFile()
    {
        $pathParts = [__DIR__, '..', 'data', 'weather_config_invalid.xls'];
        $filePath = realpath(implode(DIRECTORY_SEPARATOR, $pathParts));

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--import' => $filePath]);
        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('Temperature in Skopje', $output);
        $this->assertStringContainsString('There were no results found for:', $output);
        $this->assertStringContainsString('Temperature in Berlin', $output);
    }

    public function testErrorWithMoreThen10CityInFile()
    {
        $pathParts = [__DIR__, '..', 'data', 'weather_config_more_then_10.xls'];
        $filePath = realpath(implode(DIRECTORY_SEPARATOR, $pathParts));

        $kernel = static::createKernel();
        $application = new Application($kernel);
        $command = $application->find('app:weather-forecast');
        $commandTester = new CommandTester($command);
        $commandTester->execute(['--import' => $filePath]);
        $output = $commandTester->getDisplay();

        $this->assertStringContainsString('Config file can not have more then 10 records', $output);
    }
}